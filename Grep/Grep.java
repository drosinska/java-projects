import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Grep {
    public Grep() {}

    public static void main(String[] args) throws FileNotFoundException {

        List<String> files = new ArrayList<>(
                List.of("src/TestFile1.txt", "src/TestFile2.txt", "src/TestFile3.txt", "src/TestFile4.txt", "src/TestFile5.txt"));

        for (String file : files) {
            File f = new File(file);
            Scanner s = new Scanner(f);
            Pattern pattern = Pattern.compile("\\w*\\d+\\w*");
            String line;
            Matcher matcher;
            while (s.hasNextLine()) {
                line = s.nextLine();
                matcher = pattern.matcher(line);
                if (matcher.find()) {
                    System.out.println(file + ":" + line);
                }
            }
            s.close();
        }
    }
}
